using System.ComponentModel.DataAnnotations;

namespace webapi.Controllers.Resources
{
    public class LoginResource
    {   [Required,MaxLength(25)]
        public string userName { get; set; }
        [Required,MaxLength(25),MinLength(1)]
        public string password { get; set; }
    }
}