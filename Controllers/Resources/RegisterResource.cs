using System.ComponentModel.DataAnnotations;

namespace webapi.Controllers.Resources
{
    public class RegisterResource
    {   [Required,DataType(DataType.EmailAddress)]
        public string email { get; set; }
        [Required,MaxLength(10),MinLength(6),DataType(DataType.Password)]

        public string password { get; set; }
        [Required,MaxLength(40)]
        public string role { get; set; }
        [Required]
        public string username { get; set; }
    }
}