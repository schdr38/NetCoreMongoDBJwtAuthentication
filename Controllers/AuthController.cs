using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using webapi.Controllers.Resources;
using webapi.Core;
using webapi.Core.Models;
using webapi.Persistance;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace webapi.Controllers {
    [Route ("api/[controller]")]
    public class AuthController : ControllerBase {
        private readonly UserManager<ApplicationUser> userManager;
        private readonly SignInManager<ApplicationUser> signInManager;
        private readonly IMapper mapper;
        private readonly TokenOperations tokenOperations;

        public AuthController (UserManager<ApplicationUser> userManager,
         SignInManager<ApplicationUser> signInManager,
         IMapper mapper) {
            this.mapper = mapper;
            this.signInManager = signInManager;
            this.userManager = userManager;
            this.tokenOperations = new TokenOperations();
        }
        [HttpPost,Route("login")]
        public async Task<IActionResult> Login ([FromBody] LoginResource loginResource) {
            if (!ModelState.IsValid)
                return BadRequest (ModelState);
            var loginModel = mapper.Map<LoginResource,LoginModel>(loginResource);
             var result =await userManager.isUserExistAsync(loginModel);
             if(result == false)
                return BadRequest("Invalid username or password");
            var user = await userManager.FindByNameAsync(loginModel.userName);
            var tokenOperations = new TokenOperations();
            var token = tokenOperations.GenereteToken(user);
            return Ok(new {Token = token});
        }
        [HttpPost,Route("register")]
        public async Task<IActionResult> RegisterAsync([FromBody] RegisterResource registerResource)
        {
             if (!ModelState.IsValid)
                return BadRequest (ModelState);
            var registerModel = mapper.Map<RegisterResource,Register>(registerResource);
             var result =await userManager.FindByEmailAsync(registerModel.email);
             if(result != null)
                return BadRequest(new {Error="Kullanıcı mevcut"});
            var roles = new List<string>();
            roles.Add(registerResource.role);
            var user = new ApplicationUser{
                UserName = registerModel.userName,
                Email = registerModel.email,
                Roles= roles
            };
            var registerResult  = await userManager.CreateAsync(user,registerModel.password);
            if(registerResult.Succeeded){
            var token = tokenOperations.GenereteToken(user);
            return Ok(new {Token=token});
            }
           foreach (var error in registerResult.Errors)
           {
               ModelState.AddModelError("general",error.Description);

           }
            return BadRequest(ModelState);
        }

    }
}