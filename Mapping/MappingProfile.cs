using AutoMapper;
using webapi.Controllers.Resources;
using webapi.Core.Models;

namespace webapi.Mapping
{
    public class MappingProfile:Profile
    {
        public MappingProfile()
        {
            CreateMap<LoginResource,LoginModel>();
            CreateMap<RegisterResource,Register>();

        }
    }
}