using Microsoft.Extensions.Options;
using MongoDB.Driver;
using webapi.Core.Models;

namespace webapi.Persistance
{
    public class DatabaseContext
    {   private readonly IMongoDatabase database = null;
        public DatabaseContext(IOptions<Settings> settings)
        {
            var client = new MongoClient(settings.Value.ConnectionString);
            if(client!=null)
                database = client.GetDatabase(settings.Value.Database);
        }
        
    }
}