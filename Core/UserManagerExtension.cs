using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using webapi.Core.Models;
using webapi.Persistance;

namespace webapi.Core
{
    public static class UserManagerExtension
    {
        public static async System.Threading.Tasks.Task<bool> isUserExistAsync(this UserManager<ApplicationUser> userManager,LoginModel user)
        {   
            var appUser = await userManager.FindByNameAsync(user.userName);

            if(appUser == null)
                    return false;
            var isPasswordValid = await userManager.CheckPasswordAsync(appUser,user.password);
            if(isPasswordValid == false)
                    return false;
            return true ;
               
        }
    }
}