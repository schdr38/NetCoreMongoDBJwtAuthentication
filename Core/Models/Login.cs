namespace webapi.Core.Models
{
    public class LoginModel
    {
        public string userName { get; set; }
        public string password { get; set; }
        public string role { get; set; }
    }
}