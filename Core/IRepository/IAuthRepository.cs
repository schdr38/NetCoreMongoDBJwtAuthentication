using webapi.Core.Models;

namespace webapi.Core.IRepository
{
    public interface IAuthRepository
    {
          bool isUserExist(LoginModel login);
    }
}